package com.oneday.onedayauth.form;

import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author jiaking
 */
public class LoginForm {
//    @Size(min = 6, max = 15, message = "用户名长度应为6-15")
    private String username;

    @Size(min = 6, max = 20, message = "密码长度应为6-20")
//    @Pattern(regexp = "([A-Za-z]+[0-9]+)|([0-9]+[A-Za-z]+)", message = "密码应当同时包含字母与数字")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
