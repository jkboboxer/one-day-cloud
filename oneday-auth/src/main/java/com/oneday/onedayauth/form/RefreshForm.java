package com.oneday.onedayauth.form;

import javax.validation.constraints.NotBlank;

/**
 * @author jiaking
 * 刷新form
 */
public class RefreshForm {
    @NotBlank
    private String refreshToken;

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
