package com.oneday.onedayauth.controller;

import com.oneday.common.core.controller.BaseController;
import com.oneday.common.core.entity.AjaxResult;
import com.oneday.common.security.annotation.PreAuthenticate;
import com.oneday.common.security.service.TokenService;
import com.oneday.onedayauth.form.LoginForm;
import com.oneday.onedayauth.form.RefreshForm;
import com.oneday.onedayauth.service.UserLoginService;
import com.oneday.upms.api.dto.LoginUser;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author jiaking
 */
@RestController
public class TokenController extends BaseController {
    @Resource
    private TokenService tokenService;

    @Resource
    private UserLoginService userLoginService;
    @PostMapping("login")
    public AjaxResult login(@RequestBody@Validated LoginForm loginForm) {
        LoginUser loginUser = userLoginService.login(loginForm.getUsername(), loginForm.getPassword());
        return toAjax(tokenService.createToken(loginUser));
    }

    @GetMapping("logout")
    @PreAuthenticate
    public AjaxResult logout() {
        LoginUser currentLoginUser = tokenService.getCurrentLoginUser();
        tokenService.removeToken(currentLoginUser);
        return ok();
    }

    @PostMapping("token/refresh")
    public AjaxResult refreshToken(@RequestBody@Validated RefreshForm refreshForm) {
        return toAjax(tokenService.refreshToken(refreshForm.getRefreshToken()));
    }
}
