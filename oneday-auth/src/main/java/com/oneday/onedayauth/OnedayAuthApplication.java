package com.oneday.onedayauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = {"com.oneday"})
@EnableDiscoveryClient
public class OnedayAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnedayAuthApplication.class, args);
    }

}
