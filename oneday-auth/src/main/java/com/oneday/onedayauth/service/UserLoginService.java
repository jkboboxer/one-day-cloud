package com.oneday.onedayauth.service;

import com.oneday.common.core.constant.SecurityConstants;
import com.oneday.common.core.constant.SysUserState;
import com.oneday.common.core.entity.R;
import com.oneday.common.core.exception.NormalException;
import com.oneday.upms.api.UserApi;
import com.oneday.upms.api.dto.LoginUser;
import com.oneday.upms.api.entity.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author jiaking
 */
@Service
public class UserLoginService {
    @Resource
    private UserApi userApi;

    @Resource
    private PasswordEncoder passwordEncoder;

    public LoginUser login(String username, String password) {
        R<LoginUser> userInfo = userApi.getUserInfo(username, SecurityConstants.INNER_ACCESS);
        LoginUser data = userInfo.getData();
        if (data == null) {
            throw new NormalException("账号或密码不正确");
        }
        User user = data.getUser();
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new NormalException("账号或密码不正确");
        }
        // 清除密码
        user.setPassword(null);
        if (user.getStatus().equals(SysUserState.FORBID)) {
            throw new NormalException("账号已禁用");
        }
        return data;
    }


}
