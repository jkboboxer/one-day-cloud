package com.oneday.gateway.config;

import com.oneday.gateway.handler.ValidateCodeHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * @author jiaking
 */
@Configuration
public class HandlerConfig {
    @Bean
    public RouterFunction<ServerResponse> validateCodeRouter(ValidateCodeHandler validateCodeHandler) {
        return RouterFunctions.route(RequestPredicates.GET("/code"), validateCodeHandler);
    }
}
