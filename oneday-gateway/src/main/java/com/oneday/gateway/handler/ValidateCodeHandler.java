package com.oneday.gateway.handler;

import com.oneday.common.core.constant.CacheConstants;
import com.oneday.common.redis.service.RedisService;
import com.pig4cloud.captcha.ArithmeticCaptcha;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import sun.security.util.SecurityConstants;

import javax.annotation.Resource;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author jiaking
 */
@Component
public class ValidateCodeHandler implements HandlerFunction<ServerResponse> {
    private static final Integer DEFAULT_IMAGE_WIDTH = 100;

    private static final Integer DEFAULT_IMAGE_HEIGHT = 40;

    private static final Long CODE_TIME = 60L;

    @Resource
    private RedisService<String> redisService;

    @Override
    public Mono<ServerResponse> handle(ServerRequest serverRequest) {
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);

        String result = captcha.text();

        // 保存验证码信息
        Optional<String> randomStr = serverRequest.queryParam("codeKey");
        randomStr.ifPresent(s -> redisService.add(CacheConstants.DEFAULT_CODE_KEY + s, result,
                CODE_TIME, TimeUnit.SECONDS));

        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        captcha.out(os);

        return ServerResponse.status(HttpStatus.OK).contentType(MediaType.IMAGE_JPEG)
                .body(BodyInserters.fromResource(new ByteArrayResource(os.toByteArray())));
    }
}
