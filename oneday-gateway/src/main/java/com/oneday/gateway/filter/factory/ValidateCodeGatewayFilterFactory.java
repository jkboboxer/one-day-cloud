package com.oneday.gateway.filter.factory;

import com.oneday.common.core.constant.CacheConstants;
import com.oneday.common.redis.service.RedisService;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jiaking
 */
@Component
public class ValidateCodeGatewayFilterFactory extends AbstractNameValueGatewayFilterFactory {
    @Resource
    private RedisService<String> redisService;

    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return (exchange, chain) -> {
            String path = exchange.getRequest().getURI().getPath();
            // uri == login 并且是post请求
            if (path.equals(config.getValue())) {
                // 验证验证码
                ServerHttpRequest request = exchange.getRequest();
                List<String> codeKeyList = request.getQueryParams().get("codeKey");
                List<String> codeList = request.getQueryParams().get("code");
                if (codeKeyList == null || codeList == null) {
                    String errMsg = "{\"message\":\"参数错误\"}";
                    return errorResponse(errMsg, exchange);
                }
                String codeKey = codeKeyList.get(0);
                String code = codeList.get(0);
                String key = CacheConstants.DEFAULT_CODE_KEY + codeKey;
                String s = redisService.get(key);
                redisService.remove(key);
                if (code.equals(s)) {
                    return chain.filter(exchange);
                } else {
                    String errMsg = "{\"message\":\"验证码错误\"}";
                    return errorResponse(errMsg, exchange);
                }
            } else {
                return chain.filter(exchange);
            }
        };
    }

    private Mono<Void> errorResponse(String jsonMsg, ServerWebExchange exchange) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.BAD_REQUEST);
        response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
        return response.writeWith(Mono.create(dataBufferMonoSink -> {
            dataBufferMonoSink.success(response.bufferFactory().wrap(jsonMsg.getBytes()));
        }));
    }
}
