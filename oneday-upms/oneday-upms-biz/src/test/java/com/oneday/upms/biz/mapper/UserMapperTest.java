package com.oneday.upms.biz.mapper;

import com.oneday.upms.biz.BaseTest;
import com.oneday.upms.api.entity.User;
import org.apache.ibatis.cursor.Cursor;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author jiaking
 */
public class UserMapperTest extends BaseTest {
    @Resource
    private UserMapper userMapper;

    @Test
    @Transactional
    public void cursorTest() {
        Cursor<User> users = userMapper.selectCursor();
        users.forEach(System.out::println);
    }
}
