package com.oneday.upms.biz.service;

import com.oneday.common.redis.service.RedisService;
import com.oneday.upms.api.dto.LoginUser;
import com.oneday.upms.biz.BaseTest;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * @author jiaking
 */
public class RedisServiceTest extends BaseTest {
    @Resource
    private RedisService<LoginUser> redisService;

    @Test
    public void testRedis() {
        LoginUser loginUser = redisService.get("123");
        System.out.println(loginUser == null);
    }
}
