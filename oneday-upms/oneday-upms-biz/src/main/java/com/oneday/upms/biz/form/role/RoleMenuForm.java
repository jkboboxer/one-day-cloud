package com.oneday.upms.biz.form.role;

import java.util.List;
import java.util.Set;

/**
 * @author jiaking
 * 更新角色菜单表单
 */
public class RoleMenuForm {
    private Long id;

    private Set<Long> menuIdSet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Long> getMenuIdSet() {
        return menuIdSet;
    }

    public void setMenuIdSet(Set<Long> menuIdSet) {
        this.menuIdSet = menuIdSet;
    }
}
