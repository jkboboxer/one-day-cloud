package com.oneday.upms.biz.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oneday.upms.api.entity.Menu;
import com.oneday.upms.api.entity.RoleMenu;
import com.oneday.upms.biz.mapper.MenuMapper;
import com.oneday.upms.biz.mapper.RoleMenuMapper;
import com.oneday.upms.biz.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.oneday.upms.biz.service.IRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Resource
    private IRoleService sysRoleService;

    @Resource
    private MenuMapper menuMapper;

    @Resource
    private RoleMenuMapper roleMenuMapper;

    @Override
    public List<Menu> listByUserIdAndType(Long userId, String type) {
        List<Long> roleIdList = sysRoleService.listRoleIdByUserId(userId);
        if (roleIdList.size() > 0) {
            List<RoleMenu> roleMenus = roleMenuMapper.selectList(Wrappers.<RoleMenu>lambdaQuery().in(RoleMenu::getSysRoleId, roleIdList));
            List<Long> collect = roleMenus.stream().map(it -> {
                return it.getSysMenuId();
            }).collect(Collectors.toList());
            if (collect.size() > 0) {
                return menuMapper.selectList(Wrappers.<Menu>lambdaQuery().in(Menu::getId, collect).eq(Menu::getType, type));
            }
        }
        return new ArrayList<>();
    }

    @Override
    public List<Menu> listByRoleId(Long roleId) {
        List<RoleMenu> roleMenus = roleMenuMapper.selectList(Wrappers.<RoleMenu>lambdaQuery().eq(RoleMenu::getSysRoleId, roleId));
        Set<Long> collect = roleMenus.stream().map(RoleMenu::getSysMenuId).collect(Collectors.toSet());
        if (collect.size() == 0) {
            return new ArrayList<>();
        }
        return menuMapper.selectListByMenuIdSet(collect);
    }

    @Override
    public void deleteByIdsIncludeChildren(Long[] ids) {
        Set<Long> collect = Arrays.stream(ids).collect(Collectors.toSet());
        // 先查询到所有菜单集合（包括菜单及其子菜单）
        HashSet<Long> longs = new HashSet<>();
        selectListByIdsIncludeChildren(collect, longs);
        longs.addAll(collect);
        // 删除menu
        menuMapper.deleteBatchIds(longs);
        // 删除关联
        if (longs.size() > 0) {
            roleMenuMapper.delete(Wrappers.<RoleMenu>lambdaQuery().in(RoleMenu::getSysMenuId, longs));
        }
    }

    private void selectListByIdsIncludeChildren(Set<Long> collect, HashSet<Long> result) {
        collect.forEach(item -> {
            List<Menu> menuList = menuMapper.selectList(Wrappers.<Menu>lambdaQuery().eq(Menu::getParentId, item));
            Set<Long> set = menuList.stream().map(Menu::getId).collect(Collectors.toSet());
            result.addAll(set);
            selectListByIdsIncludeChildren(set, result);
        });
    }
}
