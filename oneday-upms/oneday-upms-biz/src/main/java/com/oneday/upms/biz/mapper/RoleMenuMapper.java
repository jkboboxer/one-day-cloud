package com.oneday.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oneday.upms.api.entity.RoleMenu;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
}
