package com.oneday.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oneday.upms.api.entity.Role;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
public interface IRoleService extends IService<Role> {

    /**
     * 获取角色列表
     * @param userId 用户id
     * @return
     */
    List<Role> listByUserId(Long userId);

    /**
     * 获取角色id列表
     * @param userId 用户id
     * @return
     */
    List<Long> listRoleIdByUserId(Long userId);
}
