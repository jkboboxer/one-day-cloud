package com.oneday.upms.biz.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oneday.upms.api.entity.Role;
import com.oneday.upms.api.entity.UserRole;
import com.oneday.upms.biz.mapper.RoleMapper;
import com.oneday.upms.biz.mapper.UserRoleMapper;
import com.oneday.upms.biz.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {
    @Resource
    private RoleMapper roleMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Override
    public List<Role> listByUserId(Long userId) {
        List<Long> longs = listRoleIdByUserId(userId);
        if (longs.size() > 0) {
            return roleMapper.selectList(Wrappers.<Role>lambdaQuery().in(Role::getId, longs));
        }
        return new ArrayList<>();
    }

    @Override
    public List<Long> listRoleIdByUserId(Long userId) {
        List<UserRole> userRoleList = userRoleMapper.selectList(Wrappers.<UserRole>lambdaQuery().eq(UserRole::getSysUserId, userId));
        return userRoleList.stream().map(it -> {
            return it.getSysRoleId();
        }).collect(Collectors.toList());
    }
}
