package com.oneday.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oneday.upms.api.entity.Role;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
public interface RoleMapper extends BaseMapper<Role> {
    @Select("select r.* from sys_role r inner join sys_user_role ur on r.id = ur.sys_role_id where ur.sys_user_id = #{userId}")
    List<Role> selectListByUserId(Long userId);
}
