package com.oneday.upms.biz.service.impl;

import com.oneday.upms.api.entity.DictItem;
import com.oneday.upms.biz.mapper.DictItemMapper;
import com.oneday.upms.biz.service.IDictItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典项 服务实现类
 * </p>
 *
 * @author xiejiaking
 * @since 2021-09-03
 */
@Service
public class DictItemServiceImpl extends ServiceImpl<DictItemMapper, DictItem> implements IDictItemService {

}
