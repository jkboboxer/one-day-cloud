package com.oneday.upms.biz.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oneday.common.core.utils.StringUtil;
import com.oneday.upms.api.entity.Menu;
import com.oneday.upms.api.entity.Role;
import com.oneday.upms.api.entity.RoleMenu;
import com.oneday.upms.api.entity.UserRole;
import com.oneday.upms.biz.mapper.MenuMapper;
import com.oneday.upms.biz.mapper.RoleMapper;
import com.oneday.upms.biz.mapper.RoleMenuMapper;
import com.oneday.upms.biz.mapper.UserRoleMapper;
import com.oneday.upms.biz.service.IPermissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author jiaking
 */
@Service
public class PermissionService implements IPermissionService {
    @Resource
    private MenuMapper menuMapper;

    @Resource
    private RoleMenuMapper roleMenuMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private RoleMapper roleMapper;
    @Override
    public Set<String> getRolePermissions(Long userId) {
        List<Role> roles = getSysRoles(userId);
        return roles.stream().map(it -> {
            return it.getRoleKey();
        }).collect(Collectors.toSet());
    }

    private List<Role> getSysRoles(Long userId) {
        List<UserRole> userRoles = userRoleMapper.selectList(Wrappers.<UserRole>lambdaQuery().eq(UserRole::getSysUserId, userId));
        Set<Long> roleIdSet = userRoles.stream().map(it -> {
            return it.getSysRoleId();
        }).collect(Collectors.toSet());
        if (roleIdSet.size() > 0) {
            return roleMapper.selectList(Wrappers.<Role>lambdaQuery().in(Role::getId, roleIdSet));
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public Set<String> getMenuPermissions(Long userId) {
        Set<String> blankStrings = new HashSet<>();
        List<Role> roles = getSysRoles(userId);
        Set<Long> sysRoleIdSet = roles.stream().map(it -> {
            return it.getId();
        }).collect(Collectors.toSet());
        if (sysRoleIdSet.size() > 0) {
            List<RoleMenu> roleMenus = roleMenuMapper.selectList(Wrappers.<RoleMenu>lambdaQuery().in(RoleMenu::getSysRoleId, sysRoleIdSet));
            Set<Long> sysMenuIdSet = roleMenus.stream().map(it -> {
                return it.getSysMenuId();
            }).collect(Collectors.toSet());
            if (sysMenuIdSet.size() > 0) {
                List<Menu> menus = menuMapper.selectList(Wrappers.<Menu>lambdaQuery().in(Menu::getId, sysMenuIdSet));
                Set<String> permissionSet = menus.stream().map(it -> {
                    return it.getPermissionKey();
                }).collect(Collectors.toSet());
//                // 菜单可代表目录、菜单、按钮 一个它可能包含多个权限 在权限key中用','分割的(暂时弃用了 不使用','分割了 一个菜单只有一个权限)
//                HashSet<String> menuPermissionSet = new HashSet<>();
//                permissionSet.forEach(it -> {
//                    if (!StringUtil.isBlank(it)) {
//                        String[] split = it.split(",");
//                        for (String s : split) {
//                            menuPermissionSet.add(s);
//                        }
//                    }
//                });
                return permissionSet;
            }
        }
        return blankStrings;

    }
}
