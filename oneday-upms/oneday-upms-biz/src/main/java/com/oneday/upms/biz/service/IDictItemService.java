package com.oneday.upms.biz.service;

import com.oneday.upms.api.entity.DictItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典项 服务类
 * </p>
 *
 * @author xiejiaking
 * @since 2021-09-03
 */
public interface IDictItemService extends IService<DictItem> {

}
