package com.oneday.upms.biz.service.impl;

import com.oneday.upms.api.entity.Dict;
import com.oneday.upms.biz.mapper.DictMapper;
import com.oneday.upms.biz.service.IDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典 服务实现类
 * </p>
 *
 * @author xiejiaking
 * @since 2021-09-03
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements IDictService {

}
