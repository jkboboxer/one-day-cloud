package com.oneday.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oneday.upms.api.entity.Menu;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
public interface IMenuService extends IService<Menu> {

    /**
     * 获取菜单列表
     * @param userId 用户id
     * @param type 菜单类型 M左菜单 B按钮
     * @return
     */
    List<Menu> listByUserIdAndType(Long userId, String type);

    /**
     * 获取菜单list
     * @param roleId 角色id
     * @return
     */
    List<Menu> listByRoleId(Long roleId);

    /**
     * 删除所有菜单包括其子菜单
     * @param ids 菜单ids
     */
    void deleteByIdsIncludeChildren(Long[] ids);
}
