package com.oneday.upms.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oneday.upms.api.dto.UserDTO;
import com.oneday.upms.api.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.cursor.Cursor;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-23
 */
public interface UserMapper extends BaseMapper<User> {
    @Select("select * from sys_user")
    Cursor<User> selectCursor();

    @Update("update sys_user set password = #{password} where id = #{id}")
    int updatePasswordById(@Param("password") String password, @Param("id") Long id);

    Page<UserDTO> selectPageUserDTO(Page page, @Param(Constants.WRAPPER) AbstractWrapper wrapper);
}
