package com.oneday.upms.biz.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oneday.common.core.entity.AjaxResult;
import com.oneday.common.core.utils.StringUtil;
import com.oneday.common.mybatis.util.WrapperUtil;
import com.oneday.common.security.annotation.PreAuthenticate;
import com.oneday.common.security.annotation.PreAuthorize;
import com.oneday.common.security.service.TokenService;
import com.oneday.upms.api.dto.LoginUser;
import com.oneday.upms.api.entity.Menu;
import com.oneday.upms.api.entity.RoleMenu;
import com.oneday.upms.biz.service.IMenuService;
import com.oneday.upms.biz.service.IRoleMenuService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.oneday.common.core.controller.BaseController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@RestController
@RequestMapping("/menu")
public class MenuController extends BaseController {
    @Resource
    private IMenuService sysMenuService;

    @Resource
    private IRoleMenuService sysRoleMenuService;

    @Resource
    private TokenService tokenService;

    /**
     * 添加菜单
     * @param menu
     * @return
     */
    @PostMapping
    @PreAuthorize(hasPermission = "admin:menu:add")
    public AjaxResult add(@RequestBody @Validated Menu menu) {
        sysMenuService.save(menu);
        return ok();
    }

    @PutMapping
    @PreAuthorize(hasPermission = "admin:menu:edit")
    public AjaxResult edit(@RequestBody @Validated Menu menu) {
        sysMenuService.updateById(menu);
        return ok();
    }

    @DeleteMapping("{ids}")
    @PreAuthorize(hasPermission = "admin:menu:delete")
    public AjaxResult delete(@PathVariable("ids") Long[] ids) {
        sysMenuService.deleteByIdsIncludeChildren(ids);

        return ok();
    }

//    /**
//     * 分页查询菜单
//     * @param page
//     * @param menu
//     * @return
//     */
//    @GetMapping("page")
//    @PreAuthorize(hasPermission = "admin:menu:query")
//    public AjaxResult page(Page<Menu> page, Menu menu) {
//        return toAjax(sysMenuService.page(page, WrapperUtil.buildQueryWrapper(menu)));
//    }

    /**
     * 菜单列表
     * @return
     */
    @GetMapping("list")
    @PreAuthorize(hasPermission = "admin:menu:query")
    public AjaxResult list(String type) {
        return toAjax(sysMenuService.list(Wrappers.<Menu>lambdaQuery().eq(!StringUtil.isBlank(type), Menu::getType, type)));
    }

    /**
     * 根据角色下的所有菜单
     * @param roleId 角色id
     * @return
     */
    @GetMapping("list/role-id/{roleId}")
    public AjaxResult listByRoleId(@PathVariable("roleId") Long roleId) {
        return toAjax(sysMenuService.listByRoleId(roleId));
    }

    /**
     * 查询当前用户的菜单列表
     * @return
     */
    @PreAuthenticate
    @GetMapping("list/me")
    public AjaxResult myMenuList(String type) {
        LoginUser currentLoginUser = tokenService.getCurrentLoginUser();
        Long userId = currentLoginUser.getUserId();
        return toAjax(sysMenuService.listByUserIdAndType(userId, type));
    }


}
