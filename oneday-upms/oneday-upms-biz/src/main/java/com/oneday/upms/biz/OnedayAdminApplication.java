package com.oneday.upms.biz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author jiaking
 */
@SpringBootApplication
@MapperScan(value = {"com.oneday.upms.biz.mapper"})
@EnableDiscoveryClient
public class OnedayAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(OnedayAdminApplication.class, args);
    }
}
