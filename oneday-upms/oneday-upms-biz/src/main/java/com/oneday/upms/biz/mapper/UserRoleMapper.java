package com.oneday.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oneday.upms.api.entity.UserRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
