package com.oneday.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oneday.upms.api.entity.UserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
public interface IUserRoleService extends IService<UserRole> {

}
