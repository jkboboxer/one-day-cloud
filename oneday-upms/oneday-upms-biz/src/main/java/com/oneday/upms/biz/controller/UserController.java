package com.oneday.upms.biz.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oneday.common.security.annotation.ICU2Null;
import com.oneday.common.core.entity.AjaxResult;
import com.oneday.common.mybatis.util.WrapperUtil;
import com.oneday.common.security.annotation.InnerAccess;
import com.oneday.common.security.annotation.PreAuthenticate;
import com.oneday.common.security.annotation.PreAuthorize;
import com.oneday.common.security.service.TokenService;
import com.oneday.upms.api.dto.LoginUser;
import com.oneday.upms.api.entity.User;
import com.oneday.upms.biz.form.UserForm;
import com.oneday.upms.biz.form.user.ResetPwdForm;
import com.oneday.upms.biz.form.user.ChangeStatusForm;
import com.oneday.upms.biz.service.IPermissionService;
import com.oneday.upms.biz.service.IRoleService;
import com.oneday.upms.biz.service.IUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.oneday.common.core.controller.BaseController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-23
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {
    @Resource
    private TokenService tokenService;

    @Resource
    private IUserService userService;

    @Resource
    private IPermissionService sysPermissionService;

    @Resource
    private PasswordEncoder passwordEncoder;

    /**
     * 查询用户登录信息
     * @param username 用户名
     * @return
     */
    @GetMapping("info/{username}")
    @InnerAccess
    public AjaxResult getUserInfo(@PathVariable("username")String username) {
        User one = userService.getOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, username));
        if (one == null) {
            return toAjax(null);
        }
        LoginUser loginUser = new LoginUser();
        loginUser.setUser(one);
        loginUser.setRoles(sysPermissionService.getRolePermissions(one.getId()));
        loginUser.setPermissions(sysPermissionService.getMenuPermissions(one.getId()));
        return toAjax(loginUser);
    }

    /**
     * 添加系统用户
     * @param userForm
     * @return
     */
    @PostMapping
    @PreAuthorize(hasPermission = "admin:user:add")
    public AjaxResult add(@RequestBody @ICU2Null UserForm userForm) {
        User user = new User();
        BeanUtils.copyProperties(userForm, user);
        userService.saveUserAndUserRole(user, userForm.getRoleIdList());
        return ok();
    }

    /**
     * 编辑系统用户
     * @param userForm (包含角色ids)
     * @return
     */
    @PutMapping
    @PreAuthorize(hasPermission = "admin:user:edit")
    public AjaxResult edit(@RequestBody@ICU2Null(id = false) UserForm userForm) {
        User user = new User();
        BeanUtils.copyProperties(userForm, user);
        userService.updateUserAndUserRole(user, userForm.getRoleIdList());
        return ok();
    }

    /**
     * 删除系统用户
     * @param userIds 用户ids
     * @return
     */
    @DeleteMapping("{userIds}")
    @PreAuthorize(hasPermission = "admin:user:delete")
    public AjaxResult delete(@PathVariable("userIds") Long[] userIds) {
        List<Long> collect = Arrays.stream(userIds).collect(Collectors.toList());
        userService.removeByIds(collect);
        return ok();
    }

    /**
     * 获取当前登录用户的信息
     * @return
     */
    @GetMapping("info/me")
    @PreAuthenticate
    public AjaxResult getMyUserInfo() {
        LoginUser currentLoginUser = tokenService.getCurrentLoginUser();
        User user = userService.getById(currentLoginUser.getUserId());
        // 清除私密信息
        user.setPassword(null);
        return toAjax(user);
    }

    @PreAuthorize(hasPermission = "admin:user:query")
    @GetMapping("page")
    public AjaxResult page(Page page, User user) {
        return toAjax(userService.pageUserDTO(page, WrapperUtil.buildQueryWrapper(user)));
    }
    
    @PreAuthorize(hasPermission = "admin:user:reset-pwd")
    @PutMapping("reset-pwd")
    public AjaxResult resetPwd(@RequestBody ResetPwdForm resetPwdForm) {
        return toAjax(userService.resetPassword(resetPwdForm.getId(), resetPwdForm.getPassword()));
    }

    @PreAuthorize(hasPermission = "admin:user:change-status")
    @PutMapping("status")
    public AjaxResult changeStatus(@RequestBody ChangeStatusForm changeStatusForm) {
        User user = new User();
        user.setId(changeStatusForm.getId());
        user.setStatus(changeStatusForm.getStatus());
        return toAjax(userService.updateById(user));
    }
}
