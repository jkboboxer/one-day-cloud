package com.oneday.upms.biz.form;

import com.oneday.upms.api.entity.User;

import java.util.List;

/**
 * @author jiaking
 */
public class UserForm extends User {
    private List<Long> roleIdList;

    public List<Long> getRoleIdList() {
        return roleIdList;
    }

    public void setRoleIdList(List<Long> roleIdList) {
        this.roleIdList = roleIdList;
    }
}
