package com.oneday.upms.biz.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oneday.upms.api.entity.Menu;
import com.oneday.upms.api.entity.RoleMenu;
import com.oneday.upms.biz.mapper.MenuMapper;
import com.oneday.upms.biz.mapper.RoleMapper;
import com.oneday.upms.biz.mapper.RoleMenuMapper;
import com.oneday.upms.biz.service.IRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {
    @Resource
    private RoleMenuMapper roleMenuMapper;
    
    @Resource
    private MenuMapper menuMapper;

    @Override
    public boolean updateRoleMenu(Long id, Set<Long> menuIdSet) {
//        // 先找menuId的所有父id 传进来的可能只有子id
//        HashSet<Long> longs = new HashSet<>();
//        menuIdSet.forEach(item -> {
//            buildMenuIdSet(item, longs);
//        });
//        menuIdSet.addAll(longs);
        int delete = roleMenuMapper.delete(Wrappers.<RoleMenu>lambdaQuery().eq(RoleMenu::getSysRoleId, id));
        menuIdSet.forEach(menuId -> {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setSysRoleId(id);
            roleMenu.setSysMenuId(menuId);
            roleMenuMapper.insert(roleMenu);
        });
        return true;
    }

    private void buildMenuIdSet(Long id, Set<Long> menuIdSet) {
        Menu menu = menuMapper.selectById(id);
        Long parentId = menu.getParentId();
        if (!parentId.equals(0)) {
            menuIdSet.add(parentId);
            buildMenuIdSet(parentId, menuIdSet);
        }
    }
}
