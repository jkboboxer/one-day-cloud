package com.oneday.upms.biz.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oneday.common.core.entity.AjaxResult;
import com.oneday.common.mybatis.util.WrapperUtil;
import com.oneday.common.security.annotation.ICU2Null;
import com.oneday.common.security.annotation.PreAuthenticate;
import com.oneday.common.security.annotation.PreAuthorize;
import com.oneday.upms.api.entity.Role;
import com.oneday.upms.api.entity.RoleMenu;
import com.oneday.upms.api.entity.UserRole;
import com.oneday.upms.biz.form.role.RoleMenuForm;
import com.oneday.upms.biz.service.IRoleMenuService;
import com.oneday.upms.biz.service.IRoleService;
import com.oneday.upms.biz.service.IUserRoleService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.oneday.common.core.controller.BaseController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {
    @Resource
    private IRoleService sysRoleService;

    @Resource
    private IRoleMenuService sysRoleMenuService;

    @Resource
    private IUserRoleService sysUserRoleService;

    /**
     * 添加系统角色
     * @param role
     * @return
     */
    @PostMapping
    @PreAuthorize(hasPermission = "admin:role:add")
    public AjaxResult add( @Validated @RequestBody @ICU2Null Role role) {
        sysRoleService.save(role);
        return ok();
    }

    /**
     * 根据id更新系统角色信息
     * @param role
     * @return
     */
    @PutMapping
    @PreAuthorize(hasPermission = "admin:role:edit")
    public AjaxResult edit(@Validated @RequestBody @ICU2Null(id = false) Role role) {
        sysRoleService.updateById(role);
        return ok();
    }

    @DeleteMapping("{ids}")
    @PreAuthorize(hasPermission = "admin:role:delete")
    public AjaxResult delete(@PathVariable("ids") Long[] ids) {
        Set<Long> collect = Arrays.stream(ids).collect(Collectors.toSet());
        if (collect.size() > 0) {
            // 删除角色菜单关联
            sysRoleMenuService.remove(Wrappers.<RoleMenu>lambdaQuery().in(RoleMenu::getSysRoleId, collect));
            // 删除用户角色关联
            sysUserRoleService.remove(Wrappers.<UserRole>lambdaQuery().in(UserRole::getSysRoleId, collect));
        }
        // 删除角色
        sysRoleService.removeByIds(collect);
        return ok();
    }

    @GetMapping("page")
    @PreAuthorize(hasPermission = "admin:role:query")
    public AjaxResult page(Page<Role> page, Role role) {
        return toAjax(sysRoleService.page(page, WrapperUtil.buildQueryWrapper(role)));
    }

    /**
     * 更新角色菜单信息(权限)
     * @param roleMenuForm
     * @return
     */
    @PreAuthorize(hasPermission = "admin:role:authority")
    @PutMapping("/menu")
    public AjaxResult updateRoleMenu(@RequestBody RoleMenuForm roleMenuForm) {
        sysRoleMenuService.updateRoleMenu(roleMenuForm.getId(), roleMenuForm.getMenuIdSet());
        return ok();
    }

    @PreAuthenticate
    @GetMapping("list")
    public AjaxResult list() {
        return toAjax(sysRoleService.list());
    }

}
