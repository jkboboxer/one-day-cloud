package com.oneday.upms.biz.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oneday.common.core.entity.AjaxResult;
import com.oneday.common.mybatis.util.WrapperUtil;
import com.oneday.common.security.annotation.ICU2Null;
import com.oneday.common.security.annotation.PreAuthorize;
import com.oneday.upms.api.entity.Dict;
import com.oneday.upms.biz.service.IDictService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.oneday.common.core.controller.BaseController;

import javax.annotation.Resource;

/**
 * <p>
 * 字典 前端控制器
 * </p>
 *
 * @author xiejiaking
 * @since 2021-09-03
 */
@RestController
@RequestMapping("/dict")
public class DictController extends BaseController {
    @Resource
    private IDictService dictService;

    /**
     * 查询字典
     * @param value 字典标记 如：user_status
     * @return
     */
    @GetMapping
    public AjaxResult query(String value) {
        return toAjax(dictService.getOne(Wrappers.<Dict>lambdaQuery().eq(Dict::getValue, value)));
    }

    @GetMapping("page")
    @PreAuthorize(hasPermission = "admin:dict:query")
    public AjaxResult page(Page<Dict> page, Dict dict) {
        return toAjax(dictService.page(page, WrapperUtil.buildQueryWrapper(dict)));
    }

    @PostMapping
    @PreAuthorize(hasPermission = "admin:dict:add")
    public AjaxResult add(@Validated @RequestBody @ICU2Null Dict dict) {
        dictService.save(dict);
        return ok();
    }

    @PutMapping
    @PreAuthorize(hasPermission = "admin:dict:edit")
    public AjaxResult edit(@Validated @RequestBody @ICU2Null(id = false) Dict dict) {
        dictService.updateById(dict);
        return ok();
    }

    @DeleteMapping("{ids}")
    @PreAuthorize(hasPermission = "admin:dict:delete")
    public AjaxResult delete(@PathVariable("ids") Long[] ids) {
        return ok();
    }

}
