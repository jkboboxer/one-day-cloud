package com.oneday.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oneday.upms.api.entity.RoleMenu;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
public interface IRoleMenuService extends IService<RoleMenu> {

    /**
     * 更新角色菜单集合
     * @param id 角色id
     * @param menuIdSet 菜单idSet
     * @return
     */
    boolean updateRoleMenu(Long id, Set<Long> menuIdSet);
}
