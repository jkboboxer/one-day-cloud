package com.oneday.upms.biz.service;

import java.util.Set;

/**
 * @author jiaking
 */
public interface IPermissionService {
    /**
     * 获取角色权限集合
     * @param userId 用户id
     * @return
     */
    Set<String> getRolePermissions(Long userId);

    /**
     * 获取菜单权限
     * @param userId 用户id
     * @return
     */
    Set<String> getMenuPermissions(Long userId);
}
