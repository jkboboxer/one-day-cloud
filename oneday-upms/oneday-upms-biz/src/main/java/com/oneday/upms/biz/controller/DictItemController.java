package com.oneday.upms.biz.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.oneday.common.core.controller.BaseController;

/**
 * <p>
 * 字典项 前端控制器
 * </p>
 *
 * @author xiejiaking
 * @since 2021-09-03
 */
@RestController
@RequestMapping("/dict-item")
public class DictItemController extends BaseController {

}
