package com.oneday.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oneday.upms.api.entity.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> selectListByMenuIdSet(@Param("menuIdSet") Set<Long> collect);
}
