package com.oneday.upms.biz.service;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.oneday.upms.api.dto.UserDTO;
import com.oneday.upms.api.entity.User;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-23
 */
public interface IUserService extends IService<User> {

    /**
     * 重置用户密码
     * @param id 用户id
     * @param password 新密码
     * @return
     */
    boolean resetPassword(Long id, String password);

    /**
     * 分页查询用户（包含角色）
     * @param page
     * @param wrapper
     * @return
     */
    Page<UserDTO> pageUserDTO(Page page, AbstractWrapper wrapper);

    /**
     * 更新用户信息（包含角色信息）
     * @param user
     * @param roleIdList
     */
    void updateUserAndUserRole(User user, List<Long> roleIdList);

    /**
     * 创建用户信息 （包含角色信息）
     * @param user
     * @param roleIdList
     */
    void saveUserAndUserRole(User user, List<Long> roleIdList);
}
