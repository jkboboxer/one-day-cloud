package com.oneday.upms.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oneday.upms.api.dto.UserDTO;
import com.oneday.upms.api.entity.User;
import com.oneday.upms.api.entity.UserRole;
import com.oneday.upms.biz.mapper.UserMapper;
import com.oneday.upms.biz.mapper.UserRoleMapper;
import com.oneday.upms.biz.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-23
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private UserMapper userMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Override
    public boolean resetPassword(Long id, String password) {
        userMapper.updatePasswordById(passwordEncoder.encode(password), id);
        return true;
    }

    @Override
    public Page<UserDTO> pageUserDTO(Page page, AbstractWrapper wrapper) {
        return userMapper.selectPageUserDTO(page, wrapper);
    }

    @Override
    @Transactional
    public void updateUserAndUserRole(User user, List<Long> roleIdList) {
        // 先删除用户角色关联
        userRoleMapper.delete(Wrappers.<UserRole>lambdaQuery().eq(UserRole::getSysUserId, user.getId()));
        userMapper.updateById(user);
        addUserRole(user.getId(), roleIdList);
    }

    private void addUserRole(Long id, List<Long> roleIdList) {
        if (roleIdList != null) {
            roleIdList.forEach(item -> {
                UserRole userRole = new UserRole();
                userRole.setSysRoleId(item);
                userRole.setSysUserId(id);
                userRoleMapper.insert(userRole);
            });
        }
    }

    @Override
    @Transactional
    public void saveUserAndUserRole(User user, List<Long> roleIdList) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userMapper.insert(user);
        addUserRole(user.getId(), roleIdList);
    }
}
