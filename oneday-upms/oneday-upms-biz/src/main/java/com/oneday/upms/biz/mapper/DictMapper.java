package com.oneday.upms.biz.mapper;

import com.oneday.upms.api.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典 Mapper 接口
 * </p>
 *
 * @author xiejiaking
 * @since 2021-09-03
 */
public interface DictMapper extends BaseMapper<Dict> {

}
