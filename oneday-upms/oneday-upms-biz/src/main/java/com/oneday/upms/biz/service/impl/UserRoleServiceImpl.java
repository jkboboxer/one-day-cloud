package com.oneday.upms.biz.service.impl;

import com.oneday.upms.api.entity.UserRole;
import com.oneday.upms.biz.mapper.UserRoleMapper;
import com.oneday.upms.biz.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
