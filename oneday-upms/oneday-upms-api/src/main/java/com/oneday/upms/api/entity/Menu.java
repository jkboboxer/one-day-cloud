package com.oneday.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.oneday.common.core.entity.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@TableName(value = "sys_menu")
public class Menu extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String title;

    /**
     * 图标
     */
    private String icon;

    /**
     * 路由
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    private Long parentId;

    /**
     * D目录 M菜单 B按钮
     */
    private String type;

    /**
     * 0正常 1停用
     */
    private String status;

    private String permissionKey;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getPermissionKey() {
        return permissionKey;
    }

    public void setPermissionKey(String permissionKey) {
        this.permissionKey = permissionKey;
    }

    @Override
    public String toString() {
        return "SysMenu{" +
            ", icon=" + icon +
            ", component=" + component +
            ", parentId=" + parentId +
            ", type=" + type +
            ", status=" + status +
            ", permissionKey=" + permissionKey +
        "}";
    }
}
