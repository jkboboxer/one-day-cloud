package com.oneday.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.oneday.common.core.entity.BaseEntity;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@TableName(value = "sys_role")
public class Role extends BaseEntity {


    private static final long serialVersionUID = 1L;

    private String name;

    private String roleKey;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @NotBlank
    public String getRoleKey() {
        return roleKey;
    }

    public void setRoleKey(String roleKey) {
        this.roleKey = roleKey;
    }

    @Override
    public String toString() {
        return "SysRole{" +
            "name=" + name +
            ", roleKey=" + roleKey +
        "}";
    }
}
