package com.oneday.upms.api.factory;

import com.oneday.upms.api.UserApi;
import com.oneday.upms.api.entity.User;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 系统用户服务降级处理
 * @author jiaking
 */
@Component
public class UserFallbackFactory implements FallbackFactory<UserApi> {
    @Override
    public UserApi create(Throwable cause) {
        return null;
    }

}
