package com.oneday.upms.api.entity;

import com.oneday.common.core.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * <p>
 * 字典
 * </p>
 *
 * @author xiejiaking
 * @since 2021-09-03
 */
@TableName("sys_dict")
public class Dict extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 字典名称
     */
    private String name;

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Dict{" +
            "name=" + name +
        "}";
    }
}
