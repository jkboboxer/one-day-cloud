package com.oneday.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.oneday.common.core.entity.BaseEntity;
import com.oneday.common.mybatis.util.QueryMode;

/**
 * <p>
 *
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-23
 */
@TableName(value = "sys_user")
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @QueryMode
    private String username;

    private String password;

    private String avatar;

    /**
     * 状态 0 正常 1禁用
     */
    private String status;

    private Boolean isAdmin;

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "username=" + username +
                ", password=" + password +
                ", avatar=" + avatar +
                "}";
    }
}

