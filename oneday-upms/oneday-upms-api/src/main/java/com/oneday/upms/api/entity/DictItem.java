package com.oneday.upms.api.entity;

import com.oneday.common.core.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * <p>
 * 字典项
 * </p>
 *
 * @author xiejiaking
 * @since 2021-09-03
 */
@TableName("sys_dict_item")
public class DictItem extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 字典标签
     */
    private String label;

    /**
     * 字典项值
     */
    private String value;

    /**
     * 字典id
     */
    private Long dictId;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public Long getDictId() {
        return dictId;
    }

    public void setDictId(Long dictId) {
        this.dictId = dictId;
    }

    @Override
    public String toString() {
        return "DictItem{" +
            "label=" + label +
            ", value=" + value +
            ", dictId=" + dictId +
        "}";
    }
}
