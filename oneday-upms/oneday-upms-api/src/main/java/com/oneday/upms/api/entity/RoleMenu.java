package com.oneday.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.oneday.common.core.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@TableName(value = "sys_role_menu")
public class RoleMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long sysRoleId;

    private Long sysMenuId;

    public Long getSysRoleId() {
        return sysRoleId;
    }

    public void setSysRoleId(Long sysRoleId) {
        this.sysRoleId = sysRoleId;
    }
    public Long getSysMenuId() {
        return sysMenuId;
    }

    public void setSysMenuId(Long sysMenuId) {
        this.sysMenuId = sysMenuId;
    }

    @Override
    public String toString() {
        return "SysRoleMenu{" +
            "sysRoleId=" + sysRoleId +
            ", sysMenuId=" + sysMenuId +
        "}";
    }
}
