package com.oneday.upms.api;

import com.oneday.common.core.constant.SecurityConstants;
import com.oneday.common.core.entity.R;
import com.oneday.upms.api.dto.LoginUser;
import com.oneday.upms.api.factory.UserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author jiaking
 */
@FeignClient(value = "admin", fallbackFactory = UserFallbackFactory.class)
public interface UserApi {
    @GetMapping("/user/info/{username}")
    R<LoginUser> getUserInfo(@PathVariable("username") String username, @RequestHeader(SecurityConstants.FORM_HEAD) String headValue);

}
