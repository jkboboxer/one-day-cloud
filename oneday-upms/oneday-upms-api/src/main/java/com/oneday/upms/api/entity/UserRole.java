package com.oneday.upms.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.oneday.common.core.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jkboboxer
 * @since 2021-07-30
 */
@TableName(value = "sys_user_role")
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long sysRoleId;

    private Long sysUserId;

    public Long getSysRoleId() {
        return sysRoleId;
    }

    public void setSysRoleId(Long sysRoleId) {
        this.sysRoleId = sysRoleId;
    }
    public Long getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    @Override
    public String toString() {
        return "SysUserRole{" +
            "sysRoleId=" + sysRoleId +
            ", sysUserId=" + sysUserId +
        "}";
    }
}
