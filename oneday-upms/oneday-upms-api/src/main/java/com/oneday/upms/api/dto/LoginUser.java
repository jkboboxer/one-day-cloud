package com.oneday.upms.api.dto;


import com.oneday.upms.api.entity.User;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

/**
 *
 * @author jiaking
 */
public class LoginUser implements Serializable {
   private static final long serialVersionUID = 1L;

   private String username;

   private Long userId;

   private String token;

   private String refreshToken;

   private Long refreshExpireTime;

   private Timestamp loginTime;

   /**
    * token过期时间
    */
   private Long expireTime;

   /**
    * 当前登录的ip地址
    */
   private String ipAddr;

   private Set<String> roles;

   private Set<String> permissions;

   /**
    * 用户信息
    */
   private User user;


   public String getRefreshToken() {
      return refreshToken;
   }

   public void setRefreshToken(String refreshToken) {
      this.refreshToken = refreshToken;
   }

   public Long getRefreshExpireTime() {
      return refreshExpireTime;
   }

   public void setRefreshExpireTime(Long refreshExpireTime) {
      this.refreshExpireTime = refreshExpireTime;
   }

   public String getUsername() {
      return username;
   }

   public void setUsername(String username) {
      this.username = username;
   }

   public Long getUserId() {
      return userId;
   }

   public void setUserId(Long userId) {
      this.userId = userId;
   }

   public String getToken() {
      return token;
   }

   public void setToken(String token) {
      this.token = token;
   }

   public Timestamp getLoginTime() {
      return loginTime;
   }

   public void setLoginTime(Timestamp loginTime) {
      this.loginTime = loginTime;
   }

   public Long getExpireTime() {
      return expireTime;
   }

   public void setExpireTime(Long expireTime) {
      this.expireTime = expireTime;
   }

   public String getIpAddr() {
      return ipAddr;
   }

   public void setIpAddr(String ipAddr) {
      this.ipAddr = ipAddr;
   }

   public Set<String> getRoles() {
      return roles;
   }

   public void setRoles(Set<String> roles) {
      this.roles = roles;
   }

   public Set<String> getPermissions() {
      return permissions;
   }

   public void setPermissions(Set<String> permissions) {
      this.permissions = permissions;
   }

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }
}
