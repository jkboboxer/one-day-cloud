package com.oneday.file.biz.service.impl;

import com.oneday.file.api.dto.FileDTO;
import com.oneday.file.biz.config.FileProperties;
import com.oneday.file.biz.minio.MinioClient;
import com.oneday.file.biz.service.IUploadService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author jiaking
 */
@Service
public class MinioUploadService implements IUploadService {
    @Resource
    private FileProperties fileProperties;

    @Resource
    private BucketService bucketService;

    @Resource
    private MinioClient minioClient;

    @Override
    public List<FileDTO> upload(MultipartFile[] files, String bucket) throws Exception {
        ArrayList<FileDTO> fileDTOS = new ArrayList<>();
        String realBucket = bucketService.validateAcquire(bucket);
        Object o = null;
        for (MultipartFile file : files) {
            FileDTO fileDTO = new FileDTO();
            String originalFilename = file.getOriginalFilename();
            fileDTO.setName(originalFilename);
            String suffix = originalFilename.substring(originalFilename.lastIndexOf('.'));
            String fileName = UUID.randomUUID().toString() + suffix;
            fileDTO.setUrl(fileProperties.getUrl() + "/" + realBucket + "/" + fileName);
            InputStream inputStream = file.getInputStream();
            String contentType = file.getContentType();
            minioClient.uploadObj(fileName, realBucket, inputStream, contentType);
            fileDTOS.add(fileDTO);
        }
        return fileDTOS;
    }
}
