package com.oneday.file.biz.constants;

/**
 * @author jiaking
 * 文件上传服务类型常量
 */
public class ServiceTypeConstants {
    public static final String DISK = "disk";

    public static final String MINIO = "minio";
}
