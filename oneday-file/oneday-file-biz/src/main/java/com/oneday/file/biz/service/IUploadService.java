package com.oneday.file.biz.service;

import com.oneday.file.api.dto.FileDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author jiaking
 * 文件上传接口
 */
public interface IUploadService {
    /**
     * 上传文件
     * @param files 文件数组
     * @param bucket  存储桶
     * @return
     */
    List<FileDTO> upload(MultipartFile[] files, String bucket) throws Exception;
}
