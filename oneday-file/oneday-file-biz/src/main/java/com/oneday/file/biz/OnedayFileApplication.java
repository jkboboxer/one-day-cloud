package com.oneday.file.biz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author jiaking
 */
@SpringBootApplication
@EnableDiscoveryClient
public class OnedayFileApplication {
    public static void main(String[] args) {
        SpringApplication.run(OnedayFileApplication.class, args);
    }

}
