package com.oneday.file.biz.config;

import com.oneday.file.biz.constants.ServiceTypeConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author jiaking
 */
@ConfigurationProperties(prefix = "file")
@Component
public class FileProperties {
    /**
     * 桶名集合
     */
    private Set<String> buckets;

    /**
     * minio访问账户
     */
    private String accessKey;

    /**
     * minio访问密码
     */
    private String secretKey;

    private String url;

    /**
     * 服务类型 默认磁盘存储类型
     */
    private String type = ServiceTypeConstants.DISK;

    /**
     * 如果是 磁盘存储类型服务 需要设置
     * 磁盘存储路径
     */
    private String disk;

    public String getDisk() {
        return disk;
    }

    public void setDisk(String disk) {
        this.disk = disk;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<String> getBuckets() {
        return buckets;
    }

    public void setBuckets(Set<String> buckets) {
        this.buckets = buckets;
    }


}
