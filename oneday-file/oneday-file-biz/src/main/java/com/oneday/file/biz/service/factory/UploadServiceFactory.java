package com.oneday.file.biz.service.factory;

import com.oneday.common.core.exception.NormalException;
import com.oneday.file.biz.config.FileProperties;
import com.oneday.file.biz.constants.ServiceTypeConstants;
import com.oneday.file.biz.service.IUploadService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author jiaking
 */
@Component
public class UploadServiceFactory {
    @Resource
    private FileProperties fileProperties;

    @Resource
    private IUploadService minioUploadService;

    @Resource
    private IUploadService diskUploadService;

    public IUploadService getUploadService() {
        switch (fileProperties.getType()) {
            case ServiceTypeConstants.MINIO: return minioUploadService;
            default: return diskUploadService;
        }
    }
}
