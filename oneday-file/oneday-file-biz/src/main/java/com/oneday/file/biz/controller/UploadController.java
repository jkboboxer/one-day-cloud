package com.oneday.file.biz.controller;

import com.oneday.common.core.controller.BaseController;
import com.oneday.common.core.entity.AjaxResult;
import com.oneday.file.biz.config.FileProperties;
import com.oneday.file.biz.service.IUploadService;
import com.oneday.file.biz.service.factory.UploadServiceFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author jiaking
 */
@RestController
public class UploadController extends BaseController {
    @Resource
    private UploadServiceFactory uploadServiceFactory;

    @PostMapping("upload")
    public AjaxResult upload(MultipartFile[] files, String bucket) throws Exception {
        IUploadService uploadService = uploadServiceFactory.getUploadService();
        return toAjax(uploadService.upload(files, bucket));
    }
}
