package com.oneday.file.biz.service.impl;

import com.oneday.file.biz.config.FileProperties;
import com.oneday.file.biz.service.IBucketService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Set;

/**
 * @author jiaking
 */
@Service
public class BucketService implements IBucketService {
    @Resource
    private FileProperties fileProperties;

    private final String defaultBucket = "default";

    @Override
    public String validateAcquire(String bucket) {
        Set<String> buckets = fileProperties.getBuckets();
        if (buckets != null && buckets.contains(bucket)) return bucket;
        else return defaultBucket;
    }
}
