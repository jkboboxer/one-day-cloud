package com.oneday.file.biz.service.impl;

import com.oneday.common.core.exception.NormalException;
import com.oneday.common.core.utils.FilePathUtil;
import com.oneday.file.api.dto.FileDTO;
import com.oneday.file.biz.config.FileProperties;
import com.oneday.file.biz.service.IUploadService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author jiaking
 */
@Service
public class DiskUploadService implements IUploadService {
    @Resource
    private FileProperties fileProperties;

    @Override
    public List<FileDTO> upload(MultipartFile[] files, String bucket) {
//        String path = FilePathUtil.concat(fileProperties.getDisk(), bucket);
        throw new NormalException("暂不支持 磁盘存储 请配置minio");
    }
}
