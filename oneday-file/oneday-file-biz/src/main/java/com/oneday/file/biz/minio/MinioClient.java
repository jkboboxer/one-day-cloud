package com.oneday.file.biz.minio;

import com.oneday.file.biz.config.FileProperties;
import com.oneday.file.biz.constants.ServiceTypeConstants;
import io.minio.*;
import io.minio.errors.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author jiaking
 */
@Component
public class MinioClient {
    @Resource
    private FileProperties fileProperties;

    private volatile io.minio.MinioClient minioClient;
    @PostConstruct
    public void post() {
        init();
    }

    private void init() {
        if (minioClient == null) {
            synchronized(MinioClient.class) {
                if (minioClient == null) {
                    if (ServiceTypeConstants.MINIO.equals(fileProperties.getType())) {
                        minioClient = io.minio.MinioClient.builder().endpoint(fileProperties.getUrl())
                                .credentials(fileProperties.getAccessKey(), fileProperties.getSecretKey())
                                .build();
                    }
                }
            }
        }
    }

    /**
     * 准备桶的提前操作
     * @param bucket
     * @return
     */
    private boolean prepareBucket(String bucket) throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        boolean b = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucket).build());
        if (!b) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucket).build());
        }
        return b;
    }

    /**
     * 上传文件对象
     * @param fileName 文件名称
     * @param bucket 桶名
     * @param inputStream 输入流
     * @param contentType 类型
     * @return
     * @throws IOException
     * @throws InvalidKeyException
     * @throws InvalidResponseException
     * @throws InsufficientDataException
     * @throws NoSuchAlgorithmException
     * @throws ServerException
     * @throws InternalException
     * @throws XmlParserException
     * @throws ErrorResponseException
     */
    public void uploadObj(String fileName, String bucket, InputStream inputStream, String contentType) throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        init();
        if (prepareBucket(bucket)) {
            ObjectWriteResponse objectWriteResponse = minioClient.putObject(PutObjectArgs.builder().object(fileName)
                    .bucket(bucket).stream(inputStream, -1, 10485760).contentType(contentType).build());
        }
    }

    public io.minio.MinioClient getMinioClient() {
        return minioClient;
    }
}
