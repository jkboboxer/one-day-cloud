package com.oneday.file.biz.service;

/**
 * @author jiaking
 */
public interface IBucketService {
    /**
     * 验证桶是否配置，如果没有配置返回默认配置
     * @param bucket 桶名
     * @return 桶名
     */
    String validateAcquire(String bucket);
}
