package com.oneday.common.security.annotation;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.Timestamp;

/**
 * @author jiaking
 */
@RestControllerAdvice
public class ICURequestBodyAdvice extends RequestBodyAdviceAdapter {
    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        ICU2Null parameterAnnotation = methodParameter.getParameterAnnotation(ICU2Null.class);
        if (parameterAnnotation != null) return true;
        return false;
    }

    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        try {
            ICU2Null parameterAnnotation = parameter.getParameterAnnotation(ICU2Null.class);
            Class<?> aClass = body.getClass();
            if (parameterAnnotation.id()) {
                Method setId = aClass.getMethod("setId", Long.class);
                setId.invoke(body, (Long)null);
            }
            if (parameterAnnotation.createTime()) {
                Method setCreateTime = aClass.getMethod("setCreateTime", Timestamp.class);
                setCreateTime.invoke(body, (Timestamp)null);
            }
            if (parameterAnnotation.updateTime()) {
                Method setUpdateTime = aClass.getMethod("setUpdateTime", Timestamp.class);
                setUpdateTime.invoke(body, (Timestamp)null);
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        return body;
    }

}
