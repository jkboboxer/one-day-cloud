package com.oneday.common.security.aspect;

import com.oneday.common.core.constant.SecurityConstants;
import com.oneday.common.core.utils.ServletUtil;
import com.oneday.common.security.annotation.InnerAccess;
import com.oneday.common.security.exception.AuthorizeFailException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;

/**
 * @author jiaking
 * 拦截内部访问请求
 */
@Aspect
public class InnerAccessAspect {

    @Around("@annotation(inner)")
    public Object around(ProceedingJoinPoint proceedingJoinPoint, InnerAccess inner) throws Throwable {
        String header = ServletUtil.getHeader(SecurityConstants.FORM_HEAD);
        if (SecurityConstants.INNER_ACCESS.equals(header))
        return proceedingJoinPoint.proceed();
        else {
            throw new AuthorizeFailException("没有权限");
        }
    }
}
