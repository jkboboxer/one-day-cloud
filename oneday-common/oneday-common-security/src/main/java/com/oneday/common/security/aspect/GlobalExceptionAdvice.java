package com.oneday.common.security.aspect;

import com.oneday.common.core.exception.NormalException;
import com.oneday.common.security.exception.AuthenticateFailException;
import com.oneday.common.security.exception.AuthorizeFailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;

/**
 * 全局异常处理
 * @author jiaking
 */
@RestControllerAdvice
public class GlobalExceptionAdvice {
    private Logger logger = LoggerFactory.getLogger(GlobalExceptionAdvice.class);

    /**
     * 参数校验错误 处理
     * @param bindException
     * @return
     */
    @ExceptionHandler(value = {BindException.class, MethodArgumentNotValidException.class, ConstraintViolationException.class})
    public ResponseEntity methodArgumentNotValidHandler(Exception bindException) {
        StringBuilder errorMsg = new StringBuilder();
        /**
         * 封装成form对象的参数验证会抛该异常-MethodArgumentNotValidException
         */
        if (bindException instanceof BindException) {
            return ResponseEntityBuilder.buildException(((BindException) bindException).getAllErrors().get(0).getDefaultMessage());
        }
        else if (bindException instanceof MethodArgumentNotValidException) {
            ((MethodArgumentNotValidException)bindException).getBindingResult().getAllErrors().stream()
                    .forEach(item -> errorMsg.append(item.getDefaultMessage()));
        } else if (bindException instanceof ConstraintViolationException) {
            /**
             * 直接在controller方法参数上使用验证会抛该异常-ConstraintViolationException
             */
            ((ConstraintViolationException)bindException).getConstraintViolations().stream()
                    .forEach(item -> errorMsg.append(item.getMessageTemplate()));
        }
        return ResponseEntityBuilder.buildException(errorMsg.toString());
    }

    @ExceptionHandler(value = {HttpMessageNotReadableException.class})
    public ResponseEntity httpMessageNotReadable(Exception exception) {
        logger.info("请求异常", exception);
        return ResponseEntityBuilder.buildException("请求内容无法读取");
    }

    /**
     * 未认证异常
     * @param bindException
     * @return
     */
    @ExceptionHandler(value = {AuthenticateFailException.class})
    public ResponseEntity authenticateExceptionHandler(Exception bindException) {
        return ResponseEntityBuilder.build(bindException.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    /**
     * 未授权异常
     * @param bindException
     * @return
     */
    @ExceptionHandler(value = {AuthorizeFailException.class})
    public ResponseEntity authorizeExceptionHandler(Exception bindException) {
        return ResponseEntityBuilder.build(bindException.getMessage(), HttpStatus.FORBIDDEN);
    }

    /**
     * 手动抛出的异常  即用户可参看详情的异常信息处理
     * @param bindException
     * @return
     */
    @ExceptionHandler(value = {NormalException.class})
    public ResponseEntity userReceiveAbleExceptionHandler(Exception bindException) {
        return ResponseEntityBuilder.buildException(bindException.getMessage());
    }

    /**
     * 请求方法异常
     * @param bindException
     * @return
     */
    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
    public ResponseEntity requestMethodExceptionHandler(Exception bindException) {
        return ResponseEntityBuilder.buildException(bindException.getMessage());
    }


    /**
     * 系统内部异常
     * @param bindException
     * @return
     */
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity internalExceptionHandler(Exception bindException) {
        String message = "系统内部错误";
        logger.error(message, bindException);
        return ResponseEntityBuilder.buildInternalError(message);
    }



    static class ResponseEntityBuilder {
        public static ResponseEntity buildException(String message) {
            return build(message, HttpStatus.BAD_REQUEST);
        }

        public static ResponseEntity buildInternalError(String message) {
            return build(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        public static ResponseEntity build(String message, HttpStatus status) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("message", message);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            return new ResponseEntity(map, httpHeaders, status);
        }
    }
}
