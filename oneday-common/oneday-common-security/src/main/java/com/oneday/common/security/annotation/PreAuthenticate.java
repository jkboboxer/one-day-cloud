package com.oneday.common.security.annotation;

import java.lang.annotation.*;

/**
 * 认证注解
 * @author jiaking
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PreAuthenticate {

}
