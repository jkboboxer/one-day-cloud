package com.oneday.common.security.exception;


/**
 * 认证失败异常
 * @author jiaking
 */
public class AuthenticateFailException extends RuntimeException {
    public AuthenticateFailException(String message) {
        super("认证失败 " + message);
    }
}
