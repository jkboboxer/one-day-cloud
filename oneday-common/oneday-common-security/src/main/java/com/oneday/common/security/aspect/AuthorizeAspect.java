package com.oneday.common.security.aspect;

import com.oneday.common.core.utils.StringUtil;
import com.oneday.common.security.annotation.PreAuthorize;
import com.oneday.common.security.exception.AuthenticateFailException;
import com.oneday.common.security.exception.AuthorizeFailException;
import com.oneday.common.security.service.TokenService;
import com.oneday.upms.api.dto.LoginUser;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import javax.annotation.Resource;
import java.util.Set;

/**
 * 授权aop
 * @author jiaking
 */
@Aspect
public class AuthorizeAspect {
    @Resource
    private TokenService tokenService;

    @Pointcut("@annotation(com.oneday.common.security.annotation.PreAuthorize)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object aroundHandle(ProceedingJoinPoint joinPoint) throws Throwable {
        // 鉴权
        authorizeHandle(joinPoint);
        return joinPoint.proceed();
    }

    private void authorizeHandle(ProceedingJoinPoint joinPoint) {
        String token = tokenService.getToken();
        if (StringUtil.isBlank(token)) {
            throw new AuthorizeFailException("token不能为空");
        }
        LoginUser currentLoginUser = tokenService.getCurrentLoginUser();
        if (currentLoginUser == null) {
            throw new AuthenticateFailException("token已过期");
        }
        /**
         * 如果是admin直接放行
         */
        if (currentLoginUser.getUser().getAdmin()) return ;

        Set<String> userPermissions = currentLoginUser.getPermissions();
        Set<String> userRoles = currentLoginUser.getRoles();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        PreAuthorize annotation = signature.getMethod().getAnnotation(PreAuthorize.class);

        String[] permissions = annotation.hasAnyPermission();
        if (permissions != null) {
            for (String permission : permissions) {
                if (userPermissions.contains(permission)) return;
            }
        }

        String permission = annotation.hasPermission();
        if (userPermissions != null && userPermissions.contains(permission)) return;

        String[] roles = annotation.hasAnyRole();
        if (roles != null) {
            for (String role : roles) {
                if (userRoles.contains(role)) return;
            }
        }

        String role = annotation.hasRole();
        if (userRoles != null && userRoles.contains(role)) return;

        throw new AuthorizeFailException("权限不足");
    }


}
