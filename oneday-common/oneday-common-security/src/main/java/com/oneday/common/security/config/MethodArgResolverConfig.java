package com.oneday.common.security.config;

import com.oneday.common.security.resolver.LoginUserHandlerMethodArgumentResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author jiaking
 */
@Configuration
public class MethodArgResolverConfig implements WebMvcConfigurer {
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        LoginUserHandlerMethodArgumentResolver loginUserHandlerMethodArgumentResolver = new LoginUserHandlerMethodArgumentResolver();
        resolvers.add(loginUserHandlerMethodArgumentResolver);
    }
}
