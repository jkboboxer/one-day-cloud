package com.oneday.common.security.service;

import com.oneday.common.core.exception.NormalException;
import com.oneday.common.core.utils.ServletUtil;
import com.oneday.common.core.utils.StringUtil;
import com.oneday.common.redis.service.RedisService;
import com.oneday.upms.api.dto.LoginUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author jiaking
 */
@Service
public class TokenService {
    @Resource
    private RedisService<LoginUser> redisService;

    @Resource
    private RedisService<String> stringRedisService;

    private final String header = "authorization";

    private final String valuePre = "bearer ";

    private final String accessToken = "access_token";

    private final Long expire = 7200L;

    private final String refreshToken = "refresh_token";

    private final Long refreshExpire = 9000L;

    /**
     * 该key的值为access_token 防止重复登录产生冗余的token
     */
    private final String usernameToken = "username_token";
    /**
     * 创建token
     * @param loginUser
     * @return
     */
    public Map<String, Object> createToken(LoginUser loginUser) {
        Map<String, Object> tokenMap = tryGetExistToken(loginUser.getUser().getUsername());
        if (tokenMap != null) return tokenMap;
        // 不存在则正常创建token
        return realCreateToken(loginUser);
    }

    private Map<String, Object> realCreateToken(LoginUser loginUser) {
        String token = UUID.randomUUID().toString();
        String refreshToken = UUID.randomUUID().toString();
        loginUser.setToken(token);
        loginUser.setRefreshToken(refreshToken);
        loginUser.setUserId(loginUser.getUser().getId());
        loginUser.setUsername(loginUser.getUser().getUsername());
        loginUser.setIpAddr(ServletUtil.getIpAddr());
        writeToken(loginUser);
        return generateTokenMap(loginUser);
    }


    /**
     * 尝试获取已经存在的token
     * @param username 用户名
     * @return
     */
    private Map<String, Object> tryGetExistToken(String username) {
        String key = usernameToken + username;
        String token = stringRedisService.get(key);
        if (token == null) return null;
        LoginUser loginUser = redisService.get(getTokenKey(token));
        if (loginUser == null) return null;
        return generateTokenMap(loginUser);
    }

    private Map<String, Object> generateTokenMap(LoginUser loginUser) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(accessToken, loginUser.getToken());
        map.put("token_expire", loginUser.getExpireTime());
        map.put(refreshToken, loginUser.getRefreshToken());
        map.put("refresh_expire", loginUser.getRefreshExpireTime());
        return map;
    }

    /**
     * 写token到redis
     * @param loginUser
     */
    public void writeToken(LoginUser loginUser) {
        loginUser.setExpireTime(expire);
        loginUser.setRefreshExpireTime(refreshExpire);
        loginUser.setLoginTime(Timestamp.valueOf(LocalDateTime.now()));

        String usernameTokenKey = usernameToken + loginUser.getUsername();
        redisService.add(usernameTokenKey, loginUser.getToken(), expire);
        String tokenKey = getTokenKey(loginUser.getToken());
        redisService.add(tokenKey, loginUser, expire);
        String refreshTokenKey = refreshToken + loginUser.getRefreshToken();
        redisService.add(refreshTokenKey, loginUser, refreshExpire);
    }

    /**
     * 刷新token
     * @param refreshTokenValue
     * @return
     */
    public Map<String, Object> refreshToken(String refreshTokenValue) {
        String key = refreshToken + refreshTokenValue;
        LoginUser loginUser = redisService.get(key);
        if (loginUser == null) throw new NormalException("refreshToken已过期");
        removeToken(loginUser);
        return realCreateToken(loginUser);
    }

    public void removeToken(LoginUser loginUser) {
        String username = loginUser.getUsername();
        String token = loginUser.getToken();
        String tokenKey = accessToken + token;
        String refreshKey = refreshToken + loginUser.getRefreshToken();
        String usernameTokenKey = usernameToken + username;
        // 重新生成token前 移除token和usernametoken 和 refreshToken
        redisService.remove(tokenKey);
        redisService.remove(usernameTokenKey);
        redisService.remove(refreshKey);
    }

    private String getTokenKey(String token) {
        return accessToken + token;
    }

    /**
     * 获取请求头中的token
     * @return
     */
    public String getToken() {
        String header = ServletUtil.getHeader(this.header);
        if (StringUtil.isBlank(header)) return null;
        return header.substring(valuePre.length());
    }

    /**
     * 获取当前登录用户信息
     * @return
     */
    public LoginUser getCurrentLoginUser() {
        String token = getToken();
        String tokenKey = getTokenKey(token);
        return redisService.get(tokenKey);
    }

}
