package com.oneday.common.security.annotation;

import java.lang.annotation.*;


/**
 * 授权注解
 * @author jiaking
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface PreAuthorize {
    /**
     * 有用某个角色
     * @return
     */
    String hasRole() default "";

    /**
     * 拥有数组中的任意一个角色
     * @return
     */
    String[] hasAnyRole() default {};

    /**
     * 拥有某种权限
     * @return
     */
    String hasPermission() default "";

    /**
     * 拥有数组中的任意一个权限
     * @return
     */
    String[] hasAnyPermission() default {};
}
