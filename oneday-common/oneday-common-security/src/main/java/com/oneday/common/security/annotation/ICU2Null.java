package com.oneday.common.security.annotation;

import java.lang.annotation.*;

/**
 * I : id
 * C : createTime
 * U : updateTime
 * '@RequestBody'注解参数的增强处理注解
 * 对使用了@RequestBody注解的参数再添加该注解
 * 可以做增强处理 将参数中的id至为null createTime、updateTime 至为null
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ICU2Null {
    boolean id() default true;
    boolean createTime() default true;
    boolean updateTime() default true;
}
