package com.oneday.common.security.exception;


/**
 * @author jiaking
 */
public class AuthorizeFailException extends RuntimeException {
    public AuthorizeFailException(String message) {
        super("授权失败 " + message);
    }
}
