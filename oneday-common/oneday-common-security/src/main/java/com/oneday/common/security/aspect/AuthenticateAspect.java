package com.oneday.common.security.aspect;

import com.oneday.common.core.utils.StringUtil;
import com.oneday.common.security.exception.AuthenticateFailException;
import com.oneday.common.security.service.TokenService;
import com.oneday.upms.api.dto.LoginUser;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import javax.annotation.Resource;

/**
 * 认证aop
 * @author jiaking
 */
@Aspect
public class AuthenticateAspect {
    @Resource
    private TokenService tokenService;

    @Pointcut("@annotation(com.oneday.common.security.annotation.PreAuthenticate)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object aroundHandle(ProceedingJoinPoint joinPoint) throws Throwable {
        // 认证
        authenticateHandle(joinPoint);
        return joinPoint.proceed();
    }

    private void authenticateHandle(ProceedingJoinPoint joinPoint) {
        String token = tokenService.getToken();
        if (StringUtil.isBlank(token)) {
            throw new AuthenticateFailException("token不能为空");
        }
        LoginUser currentLoginUser = tokenService.getCurrentLoginUser();
        if (currentLoginUser == null) {
            throw new AuthenticateFailException("token已过期");
        }
    }


}
