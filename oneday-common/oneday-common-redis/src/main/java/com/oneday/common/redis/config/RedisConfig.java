package com.oneday.common.redis.config;

import com.oneday.common.redis.service.RedisService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @author jiaking
 */
@Configuration
public class RedisConfig {
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> stringObjectRedisTemplate = new RedisTemplate<>();
        stringObjectRedisTemplate.setConnectionFactory(redisConnectionFactory);
        RedisSerializer<String> string = RedisSerializer.string();
        stringObjectRedisTemplate.setKeySerializer(string);
        stringObjectRedisTemplate.setHashKeySerializer(string);
        RedisSerializer<Object> json = RedisSerializer.json();
        stringObjectRedisTemplate.setValueSerializer(json);
        stringObjectRedisTemplate.setHashValueSerializer(json);
        return stringObjectRedisTemplate;
    }

    @Bean
    public RedisService redisService(RedisTemplate<String, Object> redisTemplate) {
        RedisService redisService = new RedisService();
        redisService.setRedisTemplate(redisTemplate);
        return redisService;
    }
}
