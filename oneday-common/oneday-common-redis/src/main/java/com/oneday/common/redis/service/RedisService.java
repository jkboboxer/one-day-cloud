package com.oneday.common.redis.service;

import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @author jiaking
 */
public class RedisService<T> {
    private RedisTemplate<String, Object> redisTemplate;

    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * 添加key value
     * @param key
     * @param value
     */
    public void add(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 添加key value 并 设置过期时间
     * 时间单位为秒
     * @param key
     * @param value
     * @param timeout
     */
    public void add(String key, Object value, Long timeout) {
        add(key, value, timeout, TimeUnit.SECONDS);
    }

    /**
     * 添加key value 并设置过期时间
     * @param key
     * @param value
     * @param timeout
     * @param timeUnit
     */
    public void add(String key, Object value, Long timeout, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
    }

    public T get(String key) {
        return (T)redisTemplate.opsForValue().get(key);
    }

    /**
     * 给key设定过期时间
     * @param key
     * @param timeout
     * @param timeUnit
     * @return
     */
    public boolean expire(String key, Long timeout, TimeUnit timeUnit) {
        return redisTemplate.expire(key, timeout, timeUnit);
    }

    /**
     * 给key设定过期时间
     * 时间单位为秒
     * @param key
     * @param timeout
     * @return
     */
    public boolean expire(String key, Long timeout) {
        return expire(key, timeout, TimeUnit.SECONDS);
    }

    /**
     * 删除指定key
     * @param key
     * @return
     */
    public boolean remove(String key) {
        return redisTemplate.delete(key);
    }
}
