package com.oneday.common.core.constant;

/**
 * @author jiaking
 */
public class SecurityConstants {
    /**
     * 请求头键 用于系统内部feign请求
     */
    public static final String FORM_HEAD = "form";

    /**
     * 内部feign请求访问许可值
     */
    public static final String INNER_ACCESS = "Y";
}
