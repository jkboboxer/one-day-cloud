package com.oneday.common.core.utils;

import java.util.ArrayList;

/**
 * @author jiaking
 */
public class StringUtil {
    public static boolean isBlank(String str) {
        if (str == null || str.trim().length() == 0) {
            return true;
        }
        return false;
    }

    public static String humpToUnderline(String str) {
        char[] chars = str.toCharArray();
        ArrayList<Integer> indexList = new ArrayList<>();
        for (int i = 1; i < chars.length; i++) {
            if (chars[i] >= 65 && chars[i] <= 90) {
                chars[i] = (char)(chars[i] + 32);
                indexList.add(i);
            }
        }
        if (indexList.size() > 0) {
            StringBuilder result = new StringBuilder();
            int begin = 0;
            for (Integer index : indexList) {
                result.append(new String(chars, begin, index - begin)).append('_');
                begin = index;
            }
            return result.append(new String(chars, begin, chars.length - begin)).toString();

        } else {
            return str;
        }
    }
}
