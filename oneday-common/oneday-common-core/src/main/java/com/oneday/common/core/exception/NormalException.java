package com.oneday.common.core.exception;

/**
 * 运行时异常
 * 由于用户的常规操作失败所导致的异常 （用户可见的异常信息，非系统内部错误导致）
 * 如：用户登录时用户名或密码输入错误
 * @author jiaking
 */
public class NormalException extends RuntimeException {
    public NormalException(String message) {
        super(message);
    }
}
