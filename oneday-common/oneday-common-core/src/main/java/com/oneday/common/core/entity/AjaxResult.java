package com.oneday.common.core.entity;

import com.oneday.common.core.constant.HttpStatus;

import java.util.HashMap;

/**
 * 前端ajax请求的返回实体
 */
public class AjaxResult extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    private static final String CODE_TAG = "code";

    /**
     * 消息
     */
    private static final String MSG_TAG = "msg";

    /**
     * 数据体
     */
    private static final String DATA_TAG = "data";

    /**
     *
     * @param code 状态码
     * @param msg 返回消息
     * @param data 实体数据
     */
    public AjaxResult(int code, String msg, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        super.put(DATA_TAG, data);
    }

    public AjaxResult put(String key, Object data) {
        super.put(key, data);
        return this;
    }

    /**
     * 返回成功的ajax请求结果
     * @param msg
     * @param data
     * @return
     */
    public static AjaxResult success(String msg, Object data) {
        return new AjaxResult(HttpStatus.SUCCESS, msg, data);
    }

    /**
     * 返回失败的ajax请求结果
     * @param msg
     * @return
     */
    public static AjaxResult fail(String msg) {
        return new AjaxResult(HttpStatus.BAD_REQUEST, msg, null);
    }

    /**
     * 返回内部错误的ajax请求结果
     * @param msg
     * @return
     */
    public static AjaxResult error(String msg) {
        return new AjaxResult(HttpStatus.ERROR, msg, null);
    }
}
