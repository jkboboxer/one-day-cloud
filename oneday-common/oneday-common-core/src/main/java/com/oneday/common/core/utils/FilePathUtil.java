package com.oneday.common.core.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * @author jiaking
 * 路径工具
 */
public class FilePathUtil {
    /**
     * 连接两个路径 如 path1=D:/data  path2=message/info  连接后 D:/data/message/info
     * @param path1 必需为前路径 不会去掉前分隔符
     * @param path2 必需为后路径 会去掉前后分隔符
     * @return
     */
    public static String concat(String path1, String path2) {
        // 去掉前后分隔符
        String separator = System.getProperty("file.separator"), separator2 = "/";

        if (path1.lastIndexOf(separator) == path1.length() - 1 || path1.lastIndexOf(separator2) == path1.length() - 1) {
            path1 = path1.substring(0, path1.length() - 1);
        }
        if (path2.indexOf(separator) == 0 || path2.indexOf(separator2) == 0) {
            path2 = path2.substring(1);
        }
        if (path2.lastIndexOf(separator) == path2.length() - 1 || path2.lastIndexOf(separator2) == path2.length() - 1) {
            path2 = path2.substring(0, path2.length() - 1);
        }
        return path1.concat(separator).concat(path2);
    }

}
