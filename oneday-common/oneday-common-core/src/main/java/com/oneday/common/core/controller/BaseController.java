package com.oneday.common.core.controller;

import com.oneday.common.core.entity.AjaxResult;

public class BaseController {

    protected AjaxResult toAjax(Object data) {
        return AjaxResult.success("操作成功", data);
    }

    protected AjaxResult ok() {
        return AjaxResult.success("操作成功", null);
    }

    protected AjaxResult ok(Object data) {
        return AjaxResult.success("操作成功", data);
    }
}
