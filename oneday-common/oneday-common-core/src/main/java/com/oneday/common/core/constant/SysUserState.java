package com.oneday.common.core.constant;

/**
 * 系统用户状态
 * @author jiaking
 */
public class SysUserState {
    /**
     * 正常
     */
    public static final String NORMAL = "0";
    /**
     * 禁用
     */
    public static final String FORBID = "1";
}
