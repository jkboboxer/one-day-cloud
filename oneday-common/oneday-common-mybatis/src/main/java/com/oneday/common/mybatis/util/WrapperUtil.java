package com.oneday.common.mybatis.util;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oneday.common.core.entity.BaseEntity;
import com.oneday.common.core.utils.StringUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author jkboboxer
 */
public class WrapperUtil {

    static Field[] getFields(Class c){
        List<Field> list = new ArrayList<>();
        if(c.getSuperclass() !=null){
            Field[] parents = getFields(c.getSuperclass());
            list.addAll(Arrays.asList(parents));
        }
        Field[] fields = c.getDeclaredFields();
        list.addAll(Arrays.asList(fields));
        return list.toArray(new Field[list.size()]);
    }
    /**
     * @param queryWrapperParam 查询参数
     * @param alias             主表别名
     * @param <T>
     * @return
     */
    public static <T> QueryWrapper<T> buildQueryWrapper(BaseEntity queryWrapperParam, String alias) {
        QueryWrapper<T> queryWrapper = Wrappers.query();
        /**
         * 获取查询参数对象属性list
         */
        Field[] declaredFields = getFields(queryWrapperParam.getClass());
        /**
         * 遍历属性list 有@QueryType注解的进行对应构造
         */
        for (Field declaredField : declaredFields) {
                buildQueryWrapper(declaredField, queryWrapper, queryWrapperParam, alias);
        }
        /**
         * params
         */
        Map<String, Object> params = queryWrapperParam.getParams();
        if (params != null) {
            String beginTime = "beginTime", endTime = "endTime";
            for (String s : params.keySet()) {
                if (s.equals(beginTime)) {
                    queryWrapper.ge("beginTime", params.get(s));
                } else if (s.equals(endTime)) {
                    queryWrapper.le("endTime", params.get(s));
                } else {
                    queryWrapper.eq(s, params.get(s));
                }
            }
        }

        /**
         * 排序 按照create_time 降序
         * 如果需要提高查询性能数据库需要建立相应的索引
         */
        queryWrapper.orderByDesc(alias != null ? alias + ".create_time" : "create_time");
        return queryWrapper;
    }


    public static <T> QueryWrapper<T> buildQueryWrapper(BaseEntity queryWrapperParam) {
        return buildQueryWrapper(queryWrapperParam, null);
    }

    private static <T> void buildQueryWrapper(Field declaredField, QueryWrapper<T> queryWrapper, BaseEntity queryWrapperParam, String alias) {
        /**
         * 获取注解
         */
        QueryMode annotation = declaredField.getAnnotation(QueryMode.class);
        /**
         * 匹配对应查询构造
         */
        Object value = getValue(declaredField, queryWrapperParam);
        /**
         * 处理特殊值
         */
        //处理字符串为空字符串，如果为空字符串那么就不用构建该属性了
        if (value instanceof String && "".equals(((String) value).trim())) {
            value = null;
        }
        String columnName = alias != null ? alias + "." + getColumnName(declaredField) : getColumnName(declaredField);
        /**
         *  有注解 且 有属性值才构建
         */
        if (annotation != null && value != null) {
            switch (annotation.value()) {
                case QueryModeType.EQ:
                    queryWrapper.eq(columnName, value);
                    break;
                case QueryModeType.GT_EQ:
                    queryWrapper.ge(columnName, value);
                    break;
                case QueryModeType.GT:
                    queryWrapper.gt(columnName, value);
                    break;
                case QueryModeType.LT_EQ:
                    queryWrapper.le(columnName, value);
                    break;
                case QueryModeType.LT:
                    queryWrapper.lt(columnName, value);
                    break;
                case QueryModeType.IN:
                    queryWrapper.in(columnName, (Object[]) value);
                    break;
                case QueryModeType.LIKE:
                    queryWrapper.like(columnName, value);
                    break;
                case QueryModeType.LIKE_START:
                    queryWrapper.likeRight(columnName, value);
                    break;
                case QueryModeType.LIKE_END:
                    queryWrapper.likeLeft(columnName, value);
                    break;
                case QueryModeType.NOT_EQ:
                    queryWrapper.ne(columnName, value);
                    break;
                default:
                    throw new RuntimeException("不支持的QueryModeType");
            }
        }
    }


    /**
     * 获取字段列名
     *
     * @param declaredField
     * @return
     */
    private static String getColumnName(Field declaredField) {
        String name = declaredField.getName();
        return StringUtil.humpToUnderline(name);
    }

    /**
     * 获取属性值
     *
     * @param declaredField
     * @param queryWrapperParam
     */
    private static Object getValue(Field declaredField, BaseEntity queryWrapperParam) {
        /**
         * 获取属性值
         */
        Object value = null;
        try {
            boolean old = declaredField.isAccessible();
            if(!old)
                declaredField.setAccessible(true);
            value = declaredField.get(queryWrapperParam);
            declaredField.setAccessible(old);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("获取属性值异常");
        }
        return value;
    }

    /**
     * 获取属性get方法
     *
     * @param declaredField
     * @param queryWrapperParam
     * @return
     */
    private static String buildGetMethod(Field declaredField, BaseEntity queryWrapperParam) {
        StringBuilder methodName = new StringBuilder("get");
        String name = declaredField.getName();
        String firstStr = name.substring(0, 1);
        String upperFirstStr = firstStr.toUpperCase();
        return methodName.append(name.replaceFirst(firstStr, upperFirstStr)).toString();
    }


}

