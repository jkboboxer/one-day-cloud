package com.oneday.common.mybatis.util;

public class QueryModeType {
    public final static String LIKE = "like";
    public final static String LIKE_START = "like-start";
    public final static String LIKE_END = "like-end";

    public final static String EQ = "=";

    public final static String NOT_EQ = "<>";

    public final static String IN = "in";

    public final static String GT = ">";

    public final static String LT = "<";

    public final static String GT_EQ = ">=";

    public final static String LT_EQ = "<=";
}
